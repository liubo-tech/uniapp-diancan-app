import Vue from 'vue'
import Vuex from 'vuex'
import {BASE_URL} from '@/common/url-config.js'

Vue.use(Vuex)

async function timLogin(user) {
	return await getApp().globalData.tim.login(user);
}

const store = new Vuex.Store({
	state: {
		/**
		 * 是否需要强制登录
		 */
		forcedLogin: false,
		hasLogin: false,
		userName: "",
		openid: '',
		avatarUrl: '',
		timReady: false
	},
	mutations: {
		async login(state, userinfo) {
			state.userName = userinfo.nickName || '新用户';
			state.hasLogin = true;
			state.openid = userinfo.openid;
			state.avatarUrl = userinfo.avatarUrl;
			
			let [err,res] = await uni.request({
				url: BASE_URL+'/im/genUserSig',
				method: 'GET',
				data: {
					identifier: userinfo.openid
				}
			});
			let promise = timLogin({userID: userinfo.openid, userSig: res.data.data});
			promise.then(function(imResponse) {
				console.log(imResponse);
			}).catch(function(imError) {
			  console.warn('login error:', imError); // 登录失败的相关信息
			});
		},
		logout(state) {
			state.userName = "";
			state.hasLogin = false;
			state.openid = "";
			state.avatarUrl = '';
		},
		timOnDeady(state) {
			state.timReady = true;
		}
	}
})

export default store
