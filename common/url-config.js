let url = '';


// uEnvDev
if (process.env.NODE_ENV === 'development') {
    url = 'http://10.1.53.16:8080';
}
// uEnvProd
if (process.env.NODE_ENV === 'production') {
    url = 'https://www.java-book.club';
}

const BASE_URL = url;

const IMG_URL = 'http://img.java-book.club/';

export {BASE_URL, IMG_URL};
